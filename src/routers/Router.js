import Backbone from 'backbone';
import app from "../app";
import ContactsListView from "../views/ContactsListView";

export default Backbone.Router.extend({
	routes: {
		"": "contacts",
		"contacts": "contacts",
	},

	initialize: function() {
		Backbone.history.start();
	},

	contacts: function () {
		app.render(new ContactsListView());
	}
});