import Backbone from "backbone";
import ContactModel from "../models/ContactModel";

const ContactsCollection = Backbone.Collection.extend({
	model: ContactModel
});

export default new ContactsCollection();