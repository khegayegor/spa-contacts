import jquery from 'jquery';
import Router from "./routers/Router";

class App {
    containerSelector;

    configure() {
        window.$ = window.jQuery = jquery;
    }

    run(selector) {
        this.containerSelector = selector;
        this.configure();
        new Router();
    }

    render(view) {
        let $container = document.querySelector(this.containerSelector);
        $container.innerHTML = '';
        $container.appendChild(view.render());
    }
}

export default new App();