import Backbone from 'backbone';
import ContactValidator from "../services/ContactValidator";

export default Backbone.Model.extend({
    initialize: function() {},
    validate: function (attrs) {
        const errors = ContactValidator.validate(attrs);

        if (errors.name || errors.phone) {
            const error = new Error("There is an error in the response from the service");
            error.contactsErrorFields = errors;
            throw error;
        }
    }
});