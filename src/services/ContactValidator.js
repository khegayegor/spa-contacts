export default new class ContactValidator {
	isEmpty(value) {
		return value === '';
	}

	isPhone(value) {
		return /^(\+?)[0-9.-]*$/.test(value)
	}

	validate(data) {
		const errors = {
			name: '',
			phone: ''
		}

		const isEmptyName = this.isEmpty(data.name);
		const isEmptyPhone = this.isEmpty(data.phone);

		if (isEmptyName) {
			errors.name = 'Имя не может быть пустым';
		}

		if (isEmptyPhone) {
			errors.phone = 'Номер не может быть пустым';
		}

		if (!isEmptyPhone && !this.isPhone(data.phone)) {
			errors.phone = 'Недопустимый формат номера';
		}

		return errors;
	}
}