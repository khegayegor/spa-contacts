export default new class ApiMock {
	responseTimeout = 500;

	wait(time) {
		return new Promise(resolve => {
			time === 0 ? process.nextTick(resolve) : setTimeout(resolve, time);
		});
	}

	async getAllContacts(timeout = this.responseTimeout) {
		await this.wait(timeout);

		return [
			this.getContact('Егоров Алексей', '+71234567890'),
			this.getContact('Жданов Антон', '89992212200'),
			this.getContact('Храмов Роман', '81234561234'),
			this.getContact('Картунов Михаил', '+7098765431'),
			this.getContact('Федоров Илья', '88294732367'),
		]
	}

	async insertContact(data, timeout = this.responseTimeout) {
		await this.wait(timeout);

		return {
			data,
			status: 'ok',
			code: 200
		}
	}

	async deleteContact(data, timeout = this.responseTimeout) {
		await this.wait(timeout);

		return {
			data,
			status: 'ok',
			code: 200
		}
	}

	async updateContact(data, timeout = this.responseTimeout) {
		await this.wait(timeout);

		return {
			data,
			status: 'ok',
			code: 200
		}
	}

	getContact(name, phone) {
		return {
			name,
			phone
		}
	}
}