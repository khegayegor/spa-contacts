import Backbone from 'backbone';
import Mustache from 'mustache';
import contactsCollection from "../collections/ContactsCollection.js";
import ContactModel from "../models/ContactModel";
import StateModel from "../models/StateModel.js";
import ApiMock from "../services/ApiMock";

import tpl from 'bundle-text:../templates/ContactFormTpl.html';

export default Backbone.View.extend({
	events: {
		"submit form": "addObject",
	},

	state: null,

	initialize: function() {
		this.template = ((template) => {
			return (params) => {
				return Mustache.render(template, params);
			};
		})(tpl);

		this.initState();

		this.listenTo(this.state, 'change', this.render);
	},

	render: function() {
		const data = {};
		data.__state = this.state.toJSON();
		this.el.innerHTML = this.template(data);

		return this.el;
	},

	initState() {
		this.state = new StateModel({
			error: null,
			name: undefined,
			phone: undefined
		}, { silent: true });
	},

	resetState() {
		this.state.set({
			error: null,
			name: undefined,
			phone: undefined
		}, { silent: false })
	},

	setStateErrors(e) {
		const errorsData = e.contactsErrorFields;
		const error = {};

		for (let prop in errorsData) {
			if (!errorsData.hasOwnProperty(prop)) {
				continue;
			}
			error[prop] = errorsData[prop];
		}

		this.state.set({ error });
	},

	addObject: function(event) {
		event.preventDefault();

		try {
			const data = {
				name: this.el.querySelector('input[name="name"]').value,
				phone: this.el.querySelector('input[name="phone"]').value
			}

			this.state.set(data, { silent: true });

			let model = new ContactModel()
			model.set(data, { validate: true });

			ApiMock.insertContact(data).then(() => {
				this.resetState();
				contactsCollection.add(model);
			});
		} catch (e) {
			if (!e.hasOwnProperty('contactsErrorFields')) {
				throw e;
			}

			this.setStateErrors(e);
		}
	}
});
