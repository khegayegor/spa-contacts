import Backbone from 'backbone';
import _ from 'underscore';
import ContactView from "./ContactView.js";
import ContactFormView from "./ContactFormView.js";
import contactsCollection  from "../collections/ContactsCollection.js";
import ApiMock from "../services/ApiMock";

import template from 'bundle-text:../templates/ContactsListTpl.html';

export default Backbone.View.extend({
	className: 'contacts-app',

	initialize: function() {
		this.template = _.template(template);

		ApiMock.getAllContacts().then((contacts) => {
			contactsCollection.add(contacts);
		});

		this.listenTo(contactsCollection, "add", this.addOne);
	},

	render() {
		this.el.innerHTML = this.template();

		let newContactFormView = new ContactFormView();
		this.el.querySelector('.contacts-add-container').appendChild(newContactFormView.render());

		return this.el;
	},

	addOne: function(model) {
		const view = new ContactView({model: model});
		this.el.querySelector('.contacts-list').appendChild(view.render());
	},
});
