import Backbone from 'backbone';
import StateModel from "../models/StateModel.js";
import Mustache from 'mustache';
import ApiMock from "../services/ApiMock";

import tpl from 'bundle-text:../templates/ContactTpl.html';

export default Backbone.View.extend({
    tagName: 'tr',

    state: null,

    events: {
        'click .delete-contact':  'onDelete',
        'click .enable-edit': 'onEditEnable',
        'click .save-changes': 'onSave',
        'click .cancel-edit': 'onEditCancel',
    },

    initialize: function () {
        this.template = ((template) => {
            return (params) => {
                return Mustache.render(template, params);
            };
        })(tpl);

        this.initState();

        this.listenTo(this.state, 'change', this.render);
        this.listenTo(this.model,'destroy', this.remove);
    },

    render: function () {
        let data = this.model.toJSON();
        data.__state = this.state.toJSON();
        this.el.innerHTML = this.template(data);

        return this.el;
    },

    initState() {
        this.state = new StateModel({
            isEditable: false,
            error: null,
            name: undefined,
            phone: undefined
        }, { silent: true });
    },

    resetState() {
        this.state.set({
            isEditable: false,
            error: null,
            name: undefined,
            phone: undefined
        }, { silent: false })
    },

    getFieldsData() {
        return {
            name: this.el.querySelector('input[name="name"]').value,
            phone: this.el.querySelector('input[name="phone"]').value
        }
    },

    onEditEnable() {
        this.state.set({
            isEditable: true,
            name: this.model.get('name'),
            phone: this.model.get('phone')
        });
    },

    onEditCancel() {
        this.resetState();
    },

    onSave() {
        const data = this.getFieldsData();

        try {
            this.state.set(data, { silent: true });

            this.model.set(data, {
                silent: true,
                validate: true
            });

            ApiMock.updateContact(data).then(() => {
                this.resetState();
            });
        } catch (e) {
            if (!e.hasOwnProperty('contactsErrorFields')) {
                throw e;
            }

            this.setStateErrors(e);
        }
    },

    onDelete: function() {
        const data = {}; // В реальности тут мог передаваться id

        ApiMock.deleteContact(data, 50).then(() => {
            this.model.destroy();
        });
    },

    setStateErrors(e) {
        const errorsData = e.contactsErrorFields;
        const error = {};

        for (let prop in errorsData) {
            if (!errorsData.hasOwnProperty(prop)) {
                continue;
            }
            error[prop] = errorsData[prop];
        }

        this.state.set({ error });
    }
});
